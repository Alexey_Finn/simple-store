﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleStore
{
    /// <summary>
    /// Корзина с покупками (покупательская транзакция)
    /// </summary>
    public class Basket
    {
        /// <summary>
        /// Репозиторий с товарами (в реальном случае, обычно они берутся БД)
        /// </summary>
        private readonly IItemRepository _itemRepository;

        /// <summary>
        /// Специальный интерфейс с методом для получения текущей даты
        /// </summary>
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        /// <summary>
        /// Специальный интерфейс, генерирующий "случайный" идентификатор
        /// </summary>
        private readonly IIdGenerator _idGenerator;       

        /// <summary>
        /// Идентификатор корзины
        /// </summary>
        private string _id;

        /// <summary>
        /// Позиции
        /// </summary>
        private List<BasketItem> _items;

        /// <summary>
        /// Дата создания корзины
        /// </summary>
        private DateTimeOffset _createdAt;

        /// <summary>
        /// Конструктор корзины
        /// </summary>
        /// <param name="itemRepository">Репозиторий с товарами</param>
        /// <param name="currentDateTimeProvider">Интерфейс для получения текущей даты</param>
        /// <param name="idGenerator">Генератор идентификаторов</param>
        public Basket(IItemRepository itemRepository, ICurrentDateTimeProvider currentDateTimeProvider, IIdGenerator idGenerator)
        {
            _itemRepository = itemRepository;
            _currentDateTimeProvider = currentDateTimeProvider;
            _idGenerator = idGenerator;
            _id = _idGenerator.GenerateId();
            _createdAt = _currentDateTimeProvider.Get();
            _items = new List<BasketItem>();
        }

        /// <summary>
        /// Сколько уже заплачено в этой транзакции
        /// </summary>
        public decimal Paid { get; private set; }

        /// <summary>
        /// Скидка на все позиции в корзине
        /// </summary>
        public decimal Discount { get; private set; }

        /// <summary>
        /// Баланс корзины: сколько осталось заплатить
        /// </summary>
        public decimal BalanceDue { get; private set; }

        /// <summary>
        /// Подытог (стоимость всех товаров в корзине без учета скидки)
        /// </summary>
        public decimal SubTotal { get; private set; }

        /// <summary>
        /// Итог (стоимость всех товаров в корзине с учетом скидки)
        /// </summary>
        public decimal Total { get; private set; }

        /// <summary>
        /// Индикатор завершенности транзакции
        /// </summary>
        public bool IsCompleted { get; private set; }

        /// <summary>
        /// Количество позиций в корзине
        /// </summary>
        public int ItemsCount => _items.Count;

        /// <summary>
        /// Добавить товар с указанным баркодом и в указанном количестве в корзину
        /// </summary>
        /// <param name="barCode">Баркод товара</param>
        /// <param name="quantity">Количество (шт.)</param>
        public void AddItem(string barCode, int quantity = 1)
        {
            if (IsCompleted)
            {
                throw new InvalidOperationException("Транзакция завершена");
            }
            if (quantity <= 0)
            {
                throw new InvalidOperationException("Количество должно быть положительным");
            }
            var item = _itemRepository.GetItemByBarCode(barCode);
            if (item == null)
            {
                throw new InvalidOperationException("Товар не найден");
            }
            var basketItem = new BasketItem(item, quantity);
            _items.Add(basketItem);
            UpdateTotals();
        }

        /// <summary>
        /// Убрать позицию с указанным номером из корзины
        /// </summary>
        /// <param name="itemNumber">Номер позиции в корзине</param>
        public void RemoveItem(int itemNumber)
        {
            if (IsCompleted)
            {
                throw new InvalidOperationException("Транзакция завершена");
            }
            if (itemNumber < 0 || itemNumber >= _items.Count)
            {
                throw new InvalidOperationException("Неверный номер позиции");
            }
            var basketItem = _items[itemNumber];
            _items.Remove(basketItem);
            UpdateTotals();
        }

        /// <summary>
        /// Применить скидку
        /// </summary>
        /// <param name="discount">Скидка в диапазоне от 0 до 1 не включительно (например, скидка 20% = 0.2)</param>
        public void ApplyDiscount(decimal discount)
        {
            if (IsCompleted)
            {
                throw new InvalidOperationException("Транзакция завершена");
            }
            if (discount < 0 || discount >= 1)
            {
                throw new InvalidOperationException("Скидка должна быть в диапазоне от 0 до 1 не включительно");
            }
            Discount = discount;
            UpdateTotals();
        }

        /// <summary>
        /// Заплатить указанное количество денег
        /// </summary>
        /// <param name="amount">Количество денег</param>
        public void Pay(decimal amount)
        {
            if (IsCompleted)
            {
                throw new InvalidOperationException("Транзакция завершена");
            }
            if (!_items.Any())
            {
                throw new InvalidOperationException("Нет товаров в корзине");
            }
            Paid += amount;
            UpdateTotals();
        }

        /// <summary>
        /// Завершить транзакцию (баланс корзины должен быть 0)
        /// </summary>
        public void Complete()
        {
            if (BalanceDue != 0)
            {
                throw new InvalidOperationException("Ненулевой баланс");
            }
            IsCompleted = true;
        }

        /// <summary>
        /// Сгенерировать покупательский чек
        /// </summary>
        /// <returns>Отформатированный чек в виде строки</returns>
        public string GenerateReceipt()
        {
            if (!IsCompleted)
            {
                throw new InvalidOperationException("Транзакция не завершена");
            }
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"Корзина #{_id:N}");
            stringBuilder.AppendLine($"Дата: {_createdAt:dd/MM/yyyy HH:mm:ss}");
            stringBuilder.AppendLine($"______________________");
            foreach (var item in _items)
            {
                stringBuilder.AppendLine($"{item.Name}: {item.Price:0.00}€ x {item.Quantity} шт = {item.Cost:0.00}€");
            }
            stringBuilder.AppendLine($"______________________");
            stringBuilder.AppendLine($"Подытог: {SubTotal:0.00}€");
            if (Discount > 0)
            {
                stringBuilder.AppendLine($"______________________");
                stringBuilder.AppendLine($"Скидка: {Discount:##%}");
            }
            stringBuilder.AppendLine($"______________________");
            stringBuilder.AppendLine($"Итог: {Total:0.00}€");
            stringBuilder.AppendLine($"______________________");
            stringBuilder.AppendLine($"Спасибо за покупку!");
            return stringBuilder.ToString();
        }

        /// <summary>
        /// Обновить баланс, итог и подытог
        /// </summary>
        private void UpdateTotals()
        {
            SubTotal = _items.Sum(i => i.Cost);
            Total = SubTotal * (1 - Discount);
            BalanceDue = Total - Paid;
        }
    }
}
