﻿namespace SimpleStore
{
    public class BasketItem
    {
        public BasketItem(Item item, int quantity)
        {
            Name = item.Name;
            Price = item.Price;
            Quantity = quantity;
        }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public int Quantity { get; set; }

        public decimal Cost => Price * Quantity;
    }
}
