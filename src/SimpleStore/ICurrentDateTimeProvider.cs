﻿using System;

namespace SimpleStore
{
    public interface ICurrentDateTimeProvider
    {
        DateTimeOffset Get();
    }
}
